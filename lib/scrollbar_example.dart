import 'package:flutter/material.dart';
import 'package:scrollbar/random_color.dart';
import 'package:scrollbar/scrollbar.dart';
import 'package:smooth_scroll_web/smooth_scroll_web.dart';

class ScrollbarExample extends StatelessWidget {
  ScrollController controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    ///DYNAMIC SIZE EXAMPLE
    return Container(
      color: Colors.red,
      child: SmoothScrollWeb(
        controller: controller,
        child: ScrollBar(
          child: _getChild(),
          controller: controller,
          visibleHeight: MediaQuery.of(context).size.height,
        ),
      ),
    );

    ///FIXED HEIGHT EXAMPLE
    return Column(
      children: [
        Container(
          height: 500,
          color: Colors.red,
          child: SmoothScrollWeb(
            controller: controller,
            child: ScrollBar(
              child: _getChild(),
              controller: controller,
              visibleHeight: 500,
              scrollThumbColor: Colors.blue.withOpacity(0.65),
              scrollbarColor: Colors.blue.withOpacity(0.25),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getChild() {
    return Container(
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller,
        child: Column(
          children: [
            for (int i = 0; i < 200; i++)
              Container(
                height: 10,
                color: RandomColor.generate(),
              ),
          ],
        ),
      ),
    );
  }
}
